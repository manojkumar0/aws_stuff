#DynamoDB max value in Column:
#DynamoDB boto3 url link: https://dynobase.dev/dynamodb-python-with-boto3/
tickets = tickets_table.scan(
FilterExpression=Attr('limit_type').eq('m0a'))['Items']
if tickets:
max_value = max(ticket['limit_value'] for ticket in tickets)
else:
max_value = 0
#-----------------------

#Scan all items in dynamoDB table:
#To get all items from DynamoDB table, you can use Scan operation. The problem is that Scan has 1 MB limit on the amount of data it will return in a request, so we need to paginate through the results in a loop.

dynamodb = boto3.resource('dynamodb', region_name=region)
table = dynamodb.Table('my-table')
response = table.scan()
# or else can apply filter expression on scanned data
response = table.scan(FilterExpression=Attr('country').eq('US') & Attr('city').eq('NYC'))

# can use extra methods: begins_with, contains

data = response['Items'] 
 while 'LastEvaluatedKey' in response:
 response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey']) 
 data.extend(response['Items'])

#------------------------

#AWS DynamoDB Streams to Lambda
def lambda_handler(event, context):
    print(event)
    #1. Iterate over each record
    try:
        for record in event['Records']:
            #2. Handle event by type
            if record['eventName'] == 'INSERT':
                handle_insert(record)
            elif record['eventName'] == 'MODIFY':
                handle_modify(record)
            elif record['eventName'] == 'REMOVE':
                handle_remove(record)
        print('------------------------')
        return "Success!"
    except Exception as e:
        print(e)

def handle_insert(record):
    print("Handling INSERT Event")
    
    #3a. Get newImage content
    newImage = record['dynamodb']['NewImage']
    
    #3b. Parse values
    newPlayerId = newImage['playerId']['S']

    #3c. Print it
    print ('New row added with playerId=' + newPlayerId)

    print("Done handling INSERT Event")

def handle_modify(record):
    print("Handling MODIFY Event")

    #3a. Parse oldImage and score
    oldImage = record['dynamodb']['OldImage']
    oldScore = oldImage['score']['N']
    
    #3b. Parse oldImage and score
    newImage = record['dynamodb']['NewImage']
    newScore = newImage['score']['N']

    #3c. Check for change
    if oldScore != newScore:
        print('Scores changed - oldScore=' + str(oldScore) + ', newScore=' + str(newScore))

    print("Done handling MODIFY Event")

def handle_remove(record):
    print("Handling REMOVE Event")

    #3a. Parse oldImage
    oldImage = record['dynamodb']['OldImage']
    
    #3b. Parse values
    oldPlayerId = oldImage['playerId']['S']

    #3c. Print it
    print ('Row removed with playerId=' + oldPlayerId)
    print("Done handling REMOVE Event")


#-------------------------------

