import json
import boto3

def lambda_handler(event, context):
    
    # it gets the event source IP address (check in Directory1 in file1 )
    #event['Records'][0]['requestParameters']['sourceIPAddress']
    
    # get obj name when obj is added to s3 bucket
    object_name=event['Records'][0]['s3']['object']['key']
    print(object_name)
    
    client=boto3.client('ses')
    
    subject=' new object added {}'.format(object_name)
    
    # here Body tag could be either Html or Text
    message={'Subject':{'Data':subject},'Body':{'Html':{'Data':'this is body'}}}
    
    # verify the email addresses
    client.send_email(Destination={'ToAddresses':['manojkumarreddy02@gmail.com']},Message=message,Source='manojkumarreddy02@gmail.com')
   