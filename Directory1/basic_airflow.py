import datetime as dt

default_args={
	'owner':'--',
	'depends_on_past':True,
	'start_date':dt.datetime(--),
	'end_date':dt.datetime(--),
	'email':[''],
	'email_on_failure':True,
	'email_on_retry':False,
	'retries':3,
	'retry_delay':dt.timedelta(minutes=9)
}

dag=DAG(
	  'name_of_dag',
      default_args=default_args,
	  schedule_interval='*/5 * * * *'
)


bash_op= BashOperator(task_id='--',
					  bash_command='echo hello'	
					  dag=dag)
					  

python_op=PythonOperator(task_id='',
						python_callable= 'python method'	
						dag=dag)					  
						

-----------------------------------------------------------------------------------

# pass parameters to PythonOperator in Airflow	--------					

First, we can use the op_args parameter which is a list of positional arguments that will get unpacked when calling the callable function.
Second, we can use the op_kwargs parameter which is a dictionary of keyword arguments that will get unpacked in the callable function.


def my_func(*op_args):
        print(op_args)
        return op_args[0]

with DAG('python_dag', description='Python DAG', schedule_interval='*/5 * * * *', start_date=datetime(2018, 11, 1), catchup=False) as dag:
        dummy_task      = DummyOperator(task_id='dummy_task', retries=3)
        python_task     = PythonOperator(task_id='python_task', python_callable=my_func, op_args=['one', 'two', 'three'])

        dummy_task >> python_task
		
		
def my_func(p1, p2, p3):
        print(p1)
        return p1

with DAG('python_dag', description='Python DAG', schedule_interval='*/5 * * * *', start_date=datetime(2018, 11, 1), catchup=False) as dag:
        dummy_task      = DummyOperator(task_id='dummy_task', retries=3)
        python_task     = PythonOperator(task_id='python_task', python_callable=my_func, op_args=['one', 'two', 'three'])

        dummy_task >> python_task


def my_func(**kwargs):
        print(kwargs)
        return kwargs['param_1']

with DAG('python_dag', description='Python DAG', schedule_interval='*/5 * * * *', start_date=datetime(2018, 11, 1), catchup=False) as dag:
        dummy_task      = DummyOperator(task_id='dummy_task', retries=3)
        python_task     = PythonOperator(task_id='python_task', python_callable=my_func, op_kwargs={'param_1': 'one', 'param_2': 'two', 'param_3': 'three'})

        dummy_task >> python_task		
		
		
---------------------------------------------------------------
# S3KeySensor is like operator
from datetime import datetime, timedelta
from airflow.models import DAG
from airflow.operators.s3_key_sensor import S3KeySensor
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
schedule = timedelta(minutes=5)
args = {
 'owner': 'airflow',
 'start_date': days_ago(1),
 'depends_on_past': False,
}
dag = DAG(apiVersion: v1
kind: Pod
metadata:
    labels:
        app: helloworld
spec:
    containers:
    - name: helloworld
      image: christianhxc/helloworld:1.0
      ports:
      - containerPort: 80
      resources:
        requests:
            cpu: 50m
        limits:
            cpu: 100m
 dag_id='s3_key_sensor_demo_dag',
 schedule_interval=schedule, 
 default_args=args
)
def new_file_detection(**kwargs):
 print("A new file has arrived in s3 bucket")
 
file_sensor = S3KeySensor(
 task_id='s3_key_sensor_task',
 poke_interval=60 * 30, # (seconds); checking file every half an hour
 timeout=60 * 60 * 12, # timeout in 12 hours
 bucket_key="s3://[bucket_name]/[key]",
 bucket_name=None,
 wildcard_match=False,
 dag=dag)
 
print_message = PythonOperator(task_id='print_message',
 provide_context=True,
 python_callable=new_file_detection,
 dag=dag)
 
file_sensor >> print_message
		