1) decrypt the env variables in lambda:
	create a kms key in IAM -- as we will be using that key for encryting( while encryting variable we need to select that particular kms key that we created)
	(It will be good to mention this code outside function as we dont keep on decryting variables-- code inside function will be called everytime)
	After creating kms key(can do even with custom key and send to aws and then create a kms key) we can encrypt the lambda variables with this kms key.
	
	from base64 import b64decode
	import os
	import boto3
	
	def lambda_handler(event,context):
		env_variable=os.env('--variable name in lambda--')
		decryted_value= boto3.client('kms').decrypt(CiphertextBlob=b64decode(env_variable))['Plaintext']
		print(decryted_value)
		
		

------------------------------------

2) dynamodb

dynamodb=boto3.client('dynamodb')
table=dynamodb.Table(TableName=---)

table.put_item(Item={
'name':'manoj',
'age':24
})

res=table.get_item(Key={ --only partition key and sort key--})
item=res['Item']

res=table.update_item(Key={},UpdateExpression='SET age=:val1',ExpressionAttributeValues={'val1':24})
item=res['Item']

// for bulk put 
with table.batch_writer as batch:
	for i in range(0,list):
		table.put(Item=list(i))


// wait for certain time until the event happens like -- table creation and deletion
client=boto3.client('dynamodb')	
waiter=client.get_waiter('table_exists')
wait=waiter.wait(TableName='manuTable') // so it waits until some time checks frequently whether manuTable has created or not. after certain checks it throws error

------------------------------------------------------------------------

3) kinesis

	client = boto3.client('kinesis')
    user = {}
    user['firstName'] = '---'
    user['lastName'] =  '---'

    response = client.put_record(
        StreamName='SourceStream',
        Data=json.dumps(user),
        PartitionKey=user['uuid']
    )
    print(response)
	
	shardId=client.
	
	response1=client.describe_stream(StreamName='')
	shardId=response1['ShardDescription']['Shards'][0]['ShardId']
	response2=client.get_shard_iterator(StreamName='', ShardId='', ShardIteratorType=TRIM.HORIZON)
	get_iterator=response2['ShardIterator']
	
	response3=client.get_records(ShardIterator=get_iterator,Limit=2)
	print(response3)  // prints that particular record
	
	while 'NextShardIterator' in response3:
		response3=client.get_records(ShardIterator=response3['NextShardIterator'],Limit=2)
		print(response3)
	--------------------------------------------------------------------------
	
	4) lambda kinesis and dynamodb (in event we get kinesis records as streams.. we need to decode and deserialise as json obj and put item in dynamodb table)

		import boto3
		import datetime


def lambda_handler(event, context):
    """
    Receive a batch of events from Kinesis and insert into our DynamoDB table
    """
    print('Received request')
    item = None
    dynamo_db = boto3.resource('dynamodb')
    table = dynamo_db.Table('demo_workflow_data')
    decoded_record_data = [base64.b64decode(record['kinesis']['data']) for record in event['Records']]
    deserialized_data = [json.loads(decoded_record) for decoded_record in decoded_record_data]

    with table.batch_writer() as batch_writer:
        for item in deserialized_data:
            # Add a processed time so we have a rough idea how far behind we are
            item['processed'] = datetime.datetime.utcnow().isoformat()
            batch_writer.put_item(Item=item)

    # Print the last item to make it easy to see how we're doing
    print(json.dumps(item))
    print('Number of records: {}'.format(str(len(deserialized_data))))
	