import boto3
connection = boto3.client('emr',region_name='us-east-1',aws_access_key_id='Access Key',aws_secret_access_key='Secret Key',)
cluster_id = connection.run_job_flow(
   Name='test_emr_job_boto3',
   LogUri='s3://priyaj',
   ReleaseLabel='emr-5.18.0',
   Instances={
       'InstanceGroups': [
           {
               'Name': "Master",
               'Market': 'ON_DEMAND',
               'InstanceRole': 'MASTER',
               'InstanceType': 'm1.xlarge',
               'InstanceCount': 1,
           },
           {
               'Name': "Slave",
               'Market': 'ON_DEMAND',
               'InstanceRole': 'CORE',
               'InstanceType': 'm1.xlarge',
               'InstanceCount': 2,
           }
       ],
       'Ec2KeyName': 'Your key_pair name',
       'KeepJobFlowAliveWhenNoSteps': True,
       'TerminationProtected': False,
       'Ec2SubnetId': 'subnet-id',
   },
   Applications=[
       {
           'Name': 'Spark'
       },
   ],
   BootstrapActions=[
       {
           'Name': 'Install Python packages like boto3',
           'ScriptBootstrapAction': {
               'Path': 's3://mybucket/code/spark/bootstrap_spark_cluster.sh'
           }
       }
   ],
   # copying main.py spark file to /home/hadoop in 1st step and then executing in 2nd step (or can run directly s3 location)
   Steps=[
   {
       'Name': 'setup - copy files',
       'ActionOnFailure': 'CANCEL_AND_WAIT',
       'HadoopJarStep': {
           'Jar': 'command-runner.jar',
           'Args': ['aws', 's3', 'cp', 'manu/main.py', '/home/hadoop/']
       }
   },
   {
       'Name': 'Run Spark',
       'ActionOnFailure': 'CANCEL_AND_WAIT',
       'HadoopJarStep': {
           'Jar': 'command-runner.jar',
           'Args': [
                   'spark-submit',
                   '--deploy-mode', 'cluster',
                   '--py-files',
                   '/home/hadoop/main.py']
       }
   }
   ],
   VisibleToAllUsers=True,
   JobFlowRole='EMR_EC2_DefaultRole',
   ServiceRole='EMR_DefaultRole',
)


# AWS lambda boto3 to terminate an EMR cluster with Cluster ID:
client = boto3.client('emr')
def lambda_handler(event, context):
         response = client.list_clusters(
             ClusterStates=[
                      'WAITING' ])

         for cluster in response['Clusters']:
                  cluster_id = cluster['Id']
                  cluster_name = cluster['Name']
          if(cluster_name == ""):
               print(f"terminating cluster {cluster_id}")
               response = client.terminate_job_flows(JobFlowIds=[cluster_id])
               print(response)
