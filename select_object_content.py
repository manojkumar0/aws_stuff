#fetches the data from provided s3 bucket file
def s3_data_fetch(s3):
    r = s3.select_object_content(
        Bucket=args_dict.get('source_bucket_name'),
        Key=args_dict.get('source_file_name'),
        ExpressionType='SQL',
        Expression=
        "select * from s3Object s where s.name in ['manoj','kumar','varun'] and"
        " city='ysr'",
        InputSerialization={
           'CSV': {
                "FileHeaderInfo": "Use",
                "FieldDelimiter": ";"
         }
        },
        OutputSerialization={
            'CSV': {
            "FieldDelimiter": ";"
     }},)

    record_data = []
    for event in r['Payload']:
        if 'Records' in event:
            record_data.append(event['Records']['Payload'])

    decode_records=''.join(r.decode("utf-8") for r in record_data)
    return decode_records
